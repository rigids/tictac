/* Globals */
var VER = 3,
	HOR = 3,
	TOTALROWS = VER * HOR,
	CHECKERS = new Array(TOTALROWS),
    WINBY	  = [[0,1,2], [3,4,5],  [6,7,8],  [0,3,6],  [1,4,7], [2,5,8],  [0,4,8], [2,4,6]],
	COMP_MOVE;

$(document).ready(function() {
	/* empty all squares */
	start_game();

	/* Process a td being clicked */
	$("td").click(function() {
		var pos = Number($(this).attr("id"));
		
		/* If the td is empty, process the click */
		if (CHECKERS[pos] == "") {
			$(this).html("X");
			CHECKERS[pos] = "X";
			
			if (full(CHECKERS)) {
				alert("It's a tie!");
				start_game();
			} else if (wins(CHECKERS, "X")) {
				alert("You win!");
				start_game();
			} else {
				
				minimax(CHECKERS, "O");
				CHECKERS[COMP_MOVE] = "O";
				$("td[id=" + COMP_MOVE + "]").html("O");
			
				if (wins(CHECKERS, "O")) {
					alert("You lost!");
					start_game();
				}
			}
		}
	});
	

});

/* Starts a new game when game has some result */
function start_game() {
	/* Clear the table */
	$("td").each(function() { 
		$(this).html("");
	});
	
	/* Clear the CHECKERS */
	for (var i = 0; i < TOTALROWS; i++) {
		CHECKERS[i] = "";
	}
}

/* For a given state of the board, returns all the available potential moves */
function get_available_potentialmoves(state) {
	var all_potentialmoves = Array.apply(null, {length: TOTALROWS}).map(Number.call, Number);
	return all_potentialmoves.filter(function(i) { return state[i] == ""; });
}

/* Given a state of the board, returns true if the board is full */
function full(state) {
	return !get_available_potentialmoves(state).length;
}

/* Given a state of the board, returns true if the specified player has won */
function wins(state, player) {
	var win;

	for (var i = 0; i < WINBY.length; i++) {
		win = true;
		for (var j = 0; j < WINBY[i].length; j++) {
			if (state[WINBY[i][j]] != player) {
				win = false;
			}
		}
		if (win) {
			return true;
		}
	}
	return false;
}

/* Given a state of the board, returns true if the board is full or a player has won */
function terminal(state) {
	return full(state) || wins(state, "X") || wins(state, "O");
}

/* Returns the value of a state of the board */
function score(state) {
	if (wins(state, "X")) {
		return 10;
	} else if (wins(state, "O")) {
		return -10;
	} else {
		return 0;
	}
}

/* Finds the optimal decision for the AI */
function minimax(state, player) {
	if ( terminal(state)) {
		return score(state);
	}
	
	var max_score,
		min_score,
		weight = [],
		potentialmoves = [],
		opponent = (player == "X") ? "O" : "X",
		successors = get_available_potentialmoves(state);
	
	for (var s in successors) {
		var possible_state = state;
		possible_state[successors[s]] = player;
		weight.push(minimax(possible_state, opponent));
		possible_state[successors[s]] = "";
		potentialmoves.push(successors[s]);	
	}
	
	if (player == "X") {
		COMP_MOVE = potentialmoves[0];
		max_score = weight[0];
		for (var s in weight) {
			if (weight[s] > max_score) {
				max_score = weight[s];
				COMP_MOVE = potentialmoves[s];
			}
		}
		return max_score;
	} else {
		COMP_MOVE = potentialmoves[0];
		min_score = weight[0];
		for (var s in weight) {
			if (weight[s] < min_score) {
				min_score = weight[s];
				COMP_MOVE = potentialmoves[s];
			}
		}
		return min_score;
	}
}